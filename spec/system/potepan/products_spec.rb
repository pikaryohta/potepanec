require 'rails_helper'

RSpec.describe 'showのユーザーの操作のテスト', type: :system do
  let(:taxon) { create(:taxon) }
  let(:product) { create(:product, taxons: [taxon]) }

  describe 'Productsにidが渡された時' do
    before do
      visit potepan_product_path(product.id)
    end

    it 'トップページへのリンクをクリックすると遷移する' do
      click_link 'Home', match: :prefer_exact
      expect(current_path).to eq potepan_path
    end

    it 'ロゴをクリックするとトップページへ遷移する' do
      click_on 'BIGBAG'
      expect(current_path).to eq potepan_path
    end
  end
end
