require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe 'GET #showの時のテスト' do
    let!(:product) { create(:product) }

    before do
      get :show, params: { id: product.id }
    end

    it '@productが期待される値を持つこと' do
      expect(assigns(:product)).to eq product
    end

    it '200レスポンスを返す' do
      expect(response).to have_http_status '200'
    end

    it '正常なレスポンスかどうか' do
      expect(response).to have_http_status(:success)
    end

    it '表示されたページがview/showである' do
      expect(response).to render_template :show
    end
  end
end
