require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe 'full_tittleのテスト' do
    subject { full_title(page_title) }

    context 'page_titleにnilが渡された場合' do
      let(:page_title) { nil }
      it '「BIGBAG Store」と表示' do
        is_expected.to eq(Constants::BASE_TITLE_NAME)
      end
    end

    context 'タイトルが渡された時' do
      let(:page_title) { 'Ruby on Rails Baseball' }
      it 'Ruby on Rails Baseball - BIGBAG Storeで表示' do
        is_expected.to eq 'Ruby on Rails Baseball' + ' ' + Constants::BASE_TITLE_NAME
      end
    end

    context '空文字が渡された時' do
      let(:page_title) { '' }
      it '「BIGBAG Store」と表示' do
        is_expected.to eq(Constants::BASE_TITLE_NAME)
      end
    end
  end
end
